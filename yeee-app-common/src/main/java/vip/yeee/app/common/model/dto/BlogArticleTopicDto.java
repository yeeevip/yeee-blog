package vip.yeee.app.common.model.dto;

import lombok.Data;

/**
 * description......
 *
 * @author https://www.yeee.vip
 * @since 2023/7/7 11:18
 */
@Data
public class BlogArticleTopicDto {

    private Long articleId;
    private Long topicId;
    private String topicName;

}
